# install rsvg-convert
brew install librsvg

# convert svg to png
rsvg-convert -h 90 028_history.svg > HISTORY.png

